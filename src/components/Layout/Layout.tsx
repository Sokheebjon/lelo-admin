import React, {useState} from "react";
import {Layout as AntLayout, Button, Badge, Avatar, Typography, Menu, Dropdown} from "antd";
import {
    ArrowRightOutlined,
    ArrowLeftOutlined,
    HomeOutlined,
    ShoppingCartOutlined,
    UserOutlined,
    LogoutOutlined
} from '@ant-design/icons';
import {Link, Outlet, useNavigate} from "react-router-dom";
import cn from "classnames";
import Sidebar from "./_components/Sidebar";
import {getCookie, removeCookie} from "../../utils/cookie";
import SaleModal from "./_components/SaleModal";


const Layout = () => {
    const [collapsed, setCollapsed] = useState(false);
    const {Header, Content, Sider} = AntLayout;
    const [isModalVisible, setIsModalVisible] = useState(false);
    const {Text} = Typography;
    const navigate = useNavigate();
    const stringUser = getCookie("user");
    const user = JSON.parse(stringUser || "");
    const handleLogOut = () => {
        removeCookie("token");
        removeCookie("user");
        window.location.href = "/login";
    }

    const menu = (
        <Menu>
            <Menu.Item>
                <div
                    style={{display: "flex", justifyContent: "start", alignItems: "center"}}>
                    <Avatar size={36} icon={<UserOutlined/>}/>
                    <Text underline strong
                          style={{marginLeft: 5}}>{user?.firstName} {user?.lastName}</Text>
                </div>
            </Menu.Item>
            <Menu.Item>
                <Button type="link" style={{display: "flex", justifyContent: "start", alignItems: "center"}}
                        onClick={handleLogOut}
                        icon={<LogoutOutlined/>}>Выйти</Button>
            </Menu.Item>
        </Menu>
    )

    return (
        <AntLayout style={{minHeight: "100vh"}}>
            <Sider trigger={null} collapsible collapsed={collapsed}>
                <Link
                    to="/"
                    className={cn("inline-flex focus:outline-none w-32 text-2xl my-4 ml-6 text-white")}
                >
                    <style>
                        @import
                        url('https://fonts.googleapis.com/css2?family=Rubik+Beastly&family=Rubik:wght@600&display=swap');
                    </style>
                    {collapsed ? <span style={{marginLeft: 5}}><HomeOutlined/></span> : "Le & Lo"}
                    {/*<Image*/}
                    {/*    src={siteSettings.logo.url}*/}
                    {/*    alt={siteSettings.logo.alt}*/}
                    {/*    height={siteSettings.logo.height}*/}
                    {/*    width={siteSettings.logo.width}*/}
                    {/*    layout="fixed"*/}
                    {/*    loading="eager"*/}
                    {/*/>*/}
                </Link>
                <SaleModal isModalVisible={isModalVisible} setIsModalVisible={setIsModalVisible}/>
                <Sidebar/>
            </Sider>
            <AntLayout className="site-layout">
                <Header className="site-layout-background"
                        style={{padding: 0, display: "flex", justifyContent: "space-between", alignItems: "center"}}>
                    <Button onClick={() => setCollapsed(!collapsed)} className="ml-4 mt-4 font-extrabold"
                            style={{display: "flex", justifyContent: "center", alignItems: "center"}}>
                        {!collapsed ? <ArrowLeftOutlined style={{fontWeight: "bold"}}/> :
                            <ArrowRightOutlined style={{fontWeight: "bold"}}/>}
                    </Button>
                    <div style={{display: "flex", justifyContent: "space-between", alignItems: "center"}}>
                        <Button style={{backgroundColor: "red", color: "white"}}
                                onClick={() => setIsModalVisible(!isModalVisible)}>
                            Добавить скидку
                        </Button>
                        <div
                            style={{paddingRight: 30, display: "flex", justifyContent: "center", alignItems: "center"}}>
                            <Dropdown overlay={menu} placement="bottom">
                                <div style={{padding: "0 20px"}}>
                                    <Avatar size={36} icon={<UserOutlined/>}/>
                                    <Text underline strong
                                          style={{
                                              color: "white",
                                              marginLeft: 5
                                          }}>{user?.firstName} {user?.lastName}</Text>
                                </div>
                            </Dropdown>
                            {/*<Badge count={10} size="small">*/}
                            {/*    <Button onClick={() => navigate("/orders")} type="link" style={{*/}
                            {/*        width: 30,*/}
                            {/*        height: 30,*/}
                            {/*        display: "flex",*/}
                            {/*        justifyContent: "center",*/}
                            {/*        alignItems: "center",*/}
                            {/*        fontSize: 22,*/}
                            {/*        borderRadius: "50%",*/}
                            {/*        color: "white",*/}
                            {/*    }}>*/}
                            {/*        <ShoppingCartOutlined/>*/}
                            {/*    </Button>*/}
                            {/*</Badge>*/}
                        </div>
                    </div>
                </Header>
                <Content
                    className="site-layout-background"
                    style={{
                        margin: '24px 16px',
                        padding: 24,
                        minHeight: 280,
                    }}
                >
                    <Outlet/>
                </Content>
            </AntLayout>
        </AntLayout>
    )
}

export default Layout;
