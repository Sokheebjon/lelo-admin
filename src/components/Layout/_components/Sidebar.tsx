import React, {useEffect, useState} from "react";
import {Menu} from "antd";
import {
    PicRightOutlined,
    AreaChartOutlined,
    DropboxOutlined,
    UserOutlined,
    SettingOutlined,
    ShoppingOutlined,
    FileImageOutlined
} from "@ant-design/icons";
import {useLocation, Link} from "react-router-dom";


const Sidebar = () => {
    const {pathname} = useLocation();
    const [activeKeys, setActiveKeys] = useState(["0"]);

    const sidebarMenu = [
        {
            key: "0",
            label: "Дашбоард",
            link: "/",
            icon: <AreaChartOutlined/>
        },
        {
            key: "1",
            label: "Баннер",
            link: "/banner",
            icon: <FileImageOutlined />
        },
        {
            key: "2",
            label: "Заказы",
            link: "/orders",
            icon: <ShoppingOutlined/>
        },
        {
            key: "3",
            label: "Пользователи",
            link: "/users",
            icon: <UserOutlined/>
        },
        {
            key: "4",
            label: "Добавить категорию",
            link: "/add-category",
            icon: <PicRightOutlined/>
        },
        {
            key: "5",
            label: "Товары",
            link: "/products",
            icon: <DropboxOutlined/>
        },
        {
            key: "6",
            label: "Добавить товар",
            link: "/add-product",
            icon: <DropboxOutlined/>
        },
        // {
        //     key: "7",
        //     label: "Настройки",
        //     link: "/settings",
        //     icon: <SettingOutlined/>
        // }
    ]

    const changeActiveKeys = (keys: any) => {
        setActiveKeys(keys);
    };

    useEffect(() => {
        sidebarMenu.forEach((i: any) => {
            if (i.items) {
                i.items.forEach((j: any) => {
                    if (j.children) {
                        j.children.forEach((ch: any) => {
                            return ch.link === pathname ? setActiveKeys([j.key, i.key, ch.key].reverse()) : null;
                        });
                    } else {
                        return j?.link === pathname ? setActiveKeys([j.key, i.key, null]) : null;
                    }
                });
            } else {
                return i?.link === pathname ? setActiveKeys([i.key, null, null]) : null;
            }
        });
    }, [pathname]);

    return (
        <Menu
            onClick={(e) => {
                changeActiveKeys(e.keyPath);
            }}
            theme="dark"
            mode="inline"
            selectedKeys={[activeKeys[0]]}
            openKeys={activeKeys}
            defaultSelectedKeys={['0']}
            onOpenChange={(e) => changeActiveKeys(e)}
        >
            {sidebarMenu?.map(({key, label, link, icon}, index) => (
                <Menu.Item title={label} key={key} icon={icon}>
                    <Link to={link}>
                        {label}
                    </Link>
                </Menu.Item>
            ))}
        </Menu>
    )
}

export default Sidebar;
