import React, {useState} from "react";
import {Button, Input, message, Modal, Table} from "antd";
import {useMutation, useQuery} from "react-query";
import {request} from "../../../services/Api";

interface SaleModalProps {
    isModalVisible: boolean;
    setIsModalVisible: (isModalVisible: boolean) => void
}

const SaleModal = ({isModalVisible, setIsModalVisible}: SaleModalProps) => {
    const [percent, setPercent] = useState();
    const [amount, setAmount] = useState();
    const addSaleMutate = useMutation(({id}: any) => request.put(`/admin/update-sale/${id}`,
        {
            amount,
            percent
        }))
    const salesList = useQuery(["get-all-sale-list"], async () => request.get(`/admin/sales`))
    const handleCancel = () => {
        setIsModalVisible(false);
    };
    const handleSubmitDiscounts = ({id}: any) => {
        const addSale = addSaleMutate.mutateAsync({id});
        addSale.then((res) => {
            message.success("Скидка успешно добавлена")
            salesList.refetch();
            // setIsModalVisible(false)
        }).catch((err) => {
            message.error("Ошибка при добавлении скидки")
        })
    };

    const columns = [
        {
            title: '№',
            dataIndex: 'count',
            key: 'count',
            render: (text: string, record: any, index: number) => index + 1
        },
        {
            title: 'Сумма',
            dataIndex: 'amount',
            key: 'amount',
            render: (text: string, record: any, index: number) => <div>
                <Input onChange={(e: any) => setAmount(e?.target?.value)}
                       style={{border: "1px solid #969faf"}}
                       type="text" defaultValue={record?.amount} suffix={"₽"}/>
            </div>
        },
        {
            title: 'Процентов',
            dataIndex: 'percent',
            key: 'percent',
            render: (text: string, record: any, index: number) => <div>
                <Input onChange={(e: any) => setPercent(e?.target?.value)}
                       style={{border: "1px solid #969faf"}} type="text" defaultValue={record?.percent} suffix={"%"}/>
            </div>
        },
        {
            title: "Действие",
            dataIndex: 'action',
            key: 'action',
            render: (text: string, record: any, index: number) => (
                <Button onClick={() => handleSubmitDiscounts({
                    id: record?._id,
                })}
                        type="primary">Обновить</Button>
            )
        }
    ];

    return (
        <div style={{paddingTop: 20}}>
            <Modal title="Добавить скидку" footer={null} destroyOnClose={true} onCancel={handleCancel} maskClosable
                   visible={isModalVisible}>
                <Table columns={columns} dataSource={salesList?.data?.data} pagination={false}/>
                {/*<Form name="sale-adder" onFinish={onFinish} autoComplete="off">*/}
                {/*    <Form.List name="discount">*/}
                {/*        {(fields, {add, remove}) => (*/}
                {/*            <>*/}
                {/*                {fields.map(({key, name, ...restField}) => (*/}
                {/*                    <div key={key} style={{display: "flex", justifyContent: "space-between"}}>*/}
                {/*                        <Col span={22} style={{display: "flex", justifyContent: "space-between"}}>*/}
                {/*                            <Form.Item*/}
                {/*                                {...restField}*/}
                {/*                                name={[name, "amount"]}*/}
                {/*                                style={{width: "100%", marginRight: 10}}*/}
                {/*                                rules={[{required: true, message: "Добавить сумму скидки"}]}*/}
                {/*                            >*/}
                {/*                                <Input type="number" placeholder="Предельная сумма скидки"*/}
                {/*                                       suffix={"₽"}/>*/}
                {/*                            </Form.Item>*/}
                {/*                            <Form.Item*/}
                {/*                                {...restField}*/}
                {/*                                name={[name, "percent"]}*/}
                {/*                                style={{width: "100%", marginLeft: 10}}*/}
                {/*                                rules={[{required: true, message: "Добавить процент скидки"}]}*/}
                {/*                            >*/}
                {/*                                <Input type="number" placeholder="Скидка в процентах" suffix={"%"}/>*/}
                {/*                            </Form.Item>*/}
                {/*                        </Col>*/}
                {/*                        <Col span={1}>*/}
                {/*                            <MinusCircleOutlined onClick={() => remove(name)}/>*/}
                {/*                        </Col>*/}
                {/*                    </div>*/}
                {/*                ))}*/}
                {/*                <Form.Item>*/}
                {/*                    <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined/>}>*/}
                {/*                        Добавить скидку*/}
                {/*                    </Button>*/}
                {/*                </Form.Item>*/}
                {/*            </>*/}
                {/*        )}*/}
                {/*    </Form.List>*/}
                {/*    <Form.Item>*/}
                {/*        <Button style={{float: "right", marginTop: 10}} type="primary" htmlType="submit">*/}
                {/*            Добавлять*/}
                {/*        </Button>*/}
                {/*    </Form.Item>*/}
                {/*</Form>*/}
            </Modal>
        </div>
    )
}


export default SaleModal;