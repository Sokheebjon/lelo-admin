import React from 'react';
import {Routes, Route, Navigate} from "react-router-dom";
import {Layout} from "./components";
import {AddCategory, AddProduct, Dashboard, Settings, Users, Orders, Login, Products, Banner} from "./pages";
import shoppingBg from "./assets/shopping-background.webp";
import {getCookie} from "./utils/cookie";

function App() {
    const auth = !!getCookie("token");
    if (auth) {
        return <AuthPages/>
    } else {
        return <NotAuthPages/>
    }
}

const AuthPages = () => {
    return (
        <div>
            <Routes>
                <Route
                    path="*"
                    element={<Navigate to="/" replace/>}
                />
                <Route path="/" element={<Layout/>}>
                    {/*<Suspense fallback={<Spin/>}>*/}
                    <Route index element={<Dashboard/>}/>
                    <Route path="/add-product" element={<AddProduct/>}/>
                    <Route path="/products" element={<Products/>}/>
                    <Route path="/add-category" element={<AddCategory/>}/>
                    <Route path="/settings" element={<Settings/>}/>
                    <Route path="/users" element={<Users/>}/>
                    <Route path="/orders" element={<Orders/>}/>
                    <Route path="/banner" element={<Banner/>}/>
                    {/*{routes?.map(({path, element, index}, key) => (*/}
                    {/*    <Route {...{path, element, index, key}} />*/}
                    {/*))}*/}
                    {/*</Suspense>*/}
                </Route>
            </Routes>
        </div>
    )
}

const NotAuthPages = () => {
    return (
        <div style={{paddingTop: 200, backgroundImage: `url(${shoppingBg})`, height: "100vh"}} className="shopping-bg">
            <Routes>
                <Route
                    path="*"
                    element={<Navigate to="/login" replace/>}
                />
                <Route path="login" element={<Login/>}/>
                {/*{routes?.map(({path, element, index}, key) => (*/}
                {/*    <Route {...{path, element, index, key}} />*/}
                {/*))}*/}
                {/*</Suspense>*/}
            </Routes>
        </div>
    )
}


export default App;
