import React from "react";
import {Button, Image, message, Table} from "antd";
import {useMutation, useQuery} from "react-query";
import {request} from "../../services/Api";
import {DeleteOutlined} from "@ant-design/icons";

const Products = () => {
    const productList = useQuery(["get-products-list"], async () => request.get(`/all-products`));
    const deleteProduct = useMutation(({id}: { id: string }) => request.delete(`/admin/delete-product/${id}`))

    const handleDeleteProduct = (id: string) => {
        const deleteAsync = deleteProduct.mutateAsync({id})
        deleteAsync.then(() => {
            message.success("Пользователь успешно удален");
            productList.refetch();
        }).catch(() => {
            productList.refetch();
            message.error("Ошибка удаления пользователя");
        })
    }

    const columns = [
        {
            key: "count",
            title: "№",
            dataIndex: "count",
            render: (text: string, record: any, index: number) => index + 1
        },
        {
            title: 'Наименование товара',
            dataIndex: 'title',
            key: 'title',
        },
        {
            title: 'Цена',
            dataIndex: 'price',
            key: 'price',
        },
        {
            title: 'Цена продажи (Sale)',
            dataIndex: 'salePrice',
            key: 'salePrice',
        },
        {
            title: 'Изображения товаров',
            dataIndex: 'photos',
            key: 'photos',
            render: (text: string, record: any, index: number) => (
                <div>
                    {/*{record?.photos?.map((photo: string) => (*/}
                    <Image width={100} src={record?.photos[0]} alt={record?.title}/>
                    {/*))}*/}
                </div>
            )
        },
        {
            title: 'Описание товара',
            dataIndex: 'description',
            key: 'description',
        },
        {
            title: 'Действия',
            dataIndex: 'actions',
            key: 'actions',
            render: (text: string, record: any, index: number) => <div>
                {/*<Button className="rounded-2xl" type="text"> <EditOutlined/> </Button>*/}
                <Button onClick={() => handleDeleteProduct(record?._id)} className="rounded-2xl"
                        type="text"><DeleteOutlined/></Button>
            </div>
        },

    ]


    return (
        <div>
            <h1 className="text-2xl font-bold mb-4">Продукты</h1>
            <Table loading={productList?.isLoading} dataSource={productList?.data?.data?.products} columns={columns}/>
        </div>
    )
}

export default Products;