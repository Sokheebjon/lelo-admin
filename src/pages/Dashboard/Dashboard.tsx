import React from "react";
import Chart from "react-apexcharts"
import {ApexOptions} from "apexcharts";
import {Card, Col, Row, Statistic} from "antd";
import {MoneyCollectOutlined, DollarOutlined, ShoppingOutlined} from "@ant-design/icons";
import {useQuery} from "react-query";
import {request} from "../../services/Api";


const Dashboard = () => {
    const year = new Date().getFullYear()
    const getDiagramStatistics = useQuery(["get-diagram-statistics", year], async () => request.get(`/admin/sale-price/statistics`, {params: {year}}))
    const getDashboardStatistics = useQuery(["get-dashboard-statistics"], async () => request.get(`/admin/dashboard`))

    const categories = getDiagramStatistics?.data?.data?.result?.reduce((accumulator: Array<any>, currentValue: any, index: number) => {
        accumulator.push(currentValue?.month)
        return accumulator
    }, []);

    const yearlyOrderCount = getDiagramStatistics?.data?.data?.result?.reduce((accumulator: Array<any>, currentValue: any, index: number) => {
        accumulator.push(currentValue?.count)
        return accumulator
    }, []);
    const sumOrderCount = getDiagramStatistics?.data?.data?.result?.reduce((accumulator: Array<any>, currentValue: any, index: number) => {
        accumulator.push(currentValue?.amount)
        return accumulator
    }, []);

    const options: ApexOptions = {
        xaxis: {
            // categories: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
            categories: categories,
        },
        legend: {
            position: "top",
            horizontalAlign: "right"
        },
        chart: {
            toolbar: {
                show: false
            }
        },
        colors: ["#fcfc03", "#3fb60e"]
    };
    const series = [
        {
            name: "Ежегодное количество заказов",
            data: yearlyOrderCount
        },
        {
            name: "Сумма годового заказа",
            data: sumOrderCount
        },
    ];


    return (
        <div>
            <h1 className="text-2xl font-bold mb-4">Дашбоард</h1>
            <Row gutter={[20, 20]}>
                <Col span={8}>
                    <Card>
                        <Statistic
                            title="Суммарная цена со всех товаров"
                            value={getDashboardStatistics?.data?.data?.totalPrice}
                            // precision={2}
                            valueStyle={{
                                color: '#3fb60e',
                                display: "flex",
                                // justifyContent: "center",
                                alignItems: "center",
                            }}
                            prefix={<div style={{paddingBottom: 5}}><DollarOutlined/></div>}
                            suffix="₽"
                        />
                    </Card>
                </Col>
                <Col span={8}>
                    <Card>
                        <Statistic
                            title="Общая цена со всех товаров со скидкой"
                            value={getDashboardStatistics?.data?.data?.salePrice}
                            valueStyle={{color: '#FF0C0C'}}
                            prefix={<div style={{paddingBottom: 5}}><MoneyCollectOutlined/></div>}
                            suffix="₽"
                        />
                    </Card>
                </Col>
                <Col span={8}>
                    <Card>
                        <Statistic
                            title="Общее количество заказов"
                            value={getDashboardStatistics?.data?.data?.count}
                            // precision={2}
                            valueStyle={{color: '#cf1322'}}
                            prefix={<div style={{paddingBottom: 5}}><ShoppingOutlined/></div>}
                        />
                    </Card>
                </Col>
                <Col lg={24} md={12}>
                    <Chart options={options} series={series} height={350}/>
                </Col>
            </Row>
        </div>
    )
}

export default Dashboard;
