import React, {useState} from "react";
import {Checkbox, message, Select, Table, Typography} from "antd";
import {useMutation, useQuery} from "react-query";
import {request} from "../../services/Api";
import NumberFormat from "react-number-format";


const Orders = () => {
    const [status, setStatus] = useState(null);
    const ordersList = useQuery(["get-orders-list", status], async () => request.get(`/admin/orders`, {params: {status}}))
    const isConfirmedUpdate = useMutation(({
                                               id,
                                               data
                                           }: { id: string, data: { isConfirmed: number } }) => request.put(`/admin/order/confirm/${id}`, data))
    const handleConfirm = (e: any, id: string) => {
        const isConfirmed = e?.target?.checked ? 1 : 0;
        const isConfirmAsync = isConfirmedUpdate.mutateAsync({id, data: {isConfirmed}})
        isConfirmAsync.then(() => {
            message.success("Заказ подтвержден");
            ordersList.refetch();
        }).catch(() => {
            ordersList.refetch();
            message.error("Ошибка подтверждения заказа");
        })
    }

    const columns = [
        {
            key: "customerFullNameName",
            title: "Имя покупателя",
            dataIndex: "customerFullName",
            render: (text: string, record: any, index: number) => {
                const {Text} = Typography;
                return <Text key={index}>{record?.userId?.firstName} {record?.userId?.lastName}</Text>
            }
        },
        {
            key: "phoneNumber",
            title: "Телефонный номер",
            dataIndex: "phoneNumber",
            render: (text: string, record: any, index: number) => {
                return <div key={index}><NumberFormat value={record?.userId?.msisdn} displayType={'text'}
                                                      format="+# (###) ### ####"/></div>
            }
        },
        {
            key: "email",
            title: "Эл. адрес",
            dataIndex: "email",
            render: (text: string, record: any, index: number) => {
                const {Text} = Typography;
                return <Text key={index}>{record?.userId?.email}</Text>
            }
        },
        {
            key: "salePrice",
            title: "Цена в продаже (sale)",
            dataIndex: "salePrice",
            render: (text: string, record: any, index: number) => {
                return <div key={index}><NumberFormat value={record?.salePrice} displayType={'text'}
                                                      thousandSeparator={" "}/></div>
            }
        },
        {
            key: "totalPrice",
            title: "Цена",
            dataIndex: "totalPrice",
            render: (text: string, record: any, index: number) => {
                return <div key={index}><NumberFormat value={record?.totalPrice} displayType={'text'}
                                                      thousandSeparator={" "}/></div>
            }
        },
        {
            key: "confirmed",
            title: <div className="text-center">Принятый</div>,
            dataIndex: "confirmed",
            render: (text: string, record: any, index: number) => {
                return <div className="text-center">
                    <Checkbox
                        onClick={(e) => handleConfirm(e, record?._id)}
                        checked={record?.isConfirmed === 1}
                        disabled={record?.isConfirmed === 1}
                    />
                </div>
            }
        }
    ];
    const columnOrders = [
        {
            key: "title",
            title: "Наименование товара",
            dataIndex: "title",
        },
        {
            key: "price",
            title: "Цена за единицу",
            dataIndex: "price",
        },
        // {
        //     key: "colors",
        //     title: "Цвета",
        //     dataIndex: "colors",
        //     width: 150,
        // },
        // {
        //     key: "ages",
        //     title: "Возраст",
        //     dataIndex: "ages",
        //     render: (text: string, record: any, index: number) => <div className="flex">
        //         {
        //             record?.ages?.map((age: string) => (
        //                 <div key={index} className="mr-3" style={{width: 25, height: 25, backgroundColor: age}}/>
        //             ))
        //         }
        //     </div>
        // },
        {
            key: "description",
            title: "Описание товара",
            dataIndex: "description",
        },
    ];
    const orderCols = [
        {
            key: "count",
            title: "№",
            dataIndex: "count",
            render: (text: string, record: any, index: number) => index + 1
        },
        {
            key: "age",
            title: "Возраст",
            dataIndex: "age",
        },
        {
            key: "color",
            title: "Цвет",
            dataIndex: "color",
            render: (text: string, record: any, index: number) => {
                const colorName = record?.color?.split("&")?.[0];
                const colorCode = record?.color?.split("&")?.[1];
                return (<div
                    style={{display: "flex", justifyContent: "space-between"}}>
                    <p>{colorName}</p>
                    <div style={{width: 20, height: 20, backgroundColor: colorCode}}/>
                </div>)
            }
        },
        {
            key: "eachPack",
            title: "В упак.",
            dataIndex: "eachPack",
        },
        {
            key: "quantity",
            title: "Количество",
            dataIndex: "quantity",
        },
        {
            key: "price",
            title: "Цена",
            dataIndex: "price",
        },

    ];


    return (
        <div>
            <div className="flex justify-between">
                <h1 className="text-2xl font-bold mb-4">Заказы</h1>
                <div>
                    <Select
                        style={{width: 200}}
                        placeholder={"Все заказы"}
                        onChange={(val) => setStatus(val)}
                        options={
                            [
                                {
                                    label: "Все заказы",
                                    value: null
                                },
                                {
                                    label: "Принятые заказы",
                                    value: "1"
                                },
                                {
                                    label: "Непринятые заказы",
                                    value: "0"
                                }
                            ]
                        }/>
                </div>
            </div>
            <div className="bg-white p-8">
                <Table
                    className="text-center"
                    columns={columns}
                    dataSource={ordersList?.data?.data}
                    loading={ordersList?.isLoading}
                    rowKey={(record) => record?._id}
                    expandable={{
                        expandedRowRender: record =>
                            <div style={{paddingBottom: 10}} key={record?._id}>
                                <Table
                                    columns={columnOrders}
                                    dataSource={record?.orders}
                                    rowKey={(record) => record?.photos}
                                    pagination={false}
                                    expandable={{
                                        expandRowByClick: true,
                                        expandedRowRender: record => <div>
                                            <Table columns={orderCols} dataSource={record?.order} pagination={false}/>
                                            <h2 className="text-2xl text-center py-3">Изображения товара</h2>
                                            <div
                                                style={{
                                                    display: "flex",
                                                    alignItems: "center",
                                                    justifyContent: "space-around",
                                                    flexDirection: "row"
                                                }}>
                                                <img src={record?.photos} alt={record?.title}/>
                                            </div>
                                        </div>
                                    }}
                                />
                            </div>,
                        rowExpandable: (record) => record?._id
                    }}
                />
            </div>
        </div>
    )
}

export default Orders;