import React, {useState} from "react";
import {Button, message, Spin, Table} from "antd";
import {DeleteOutlined, EditOutlined, EyeOutlined, EyeInvisibleOutlined} from "@ant-design/icons";
import {useMutation, useQuery} from "react-query";
import {request} from "../../services/Api";


const Users = () => {
    // const [hidePassword, setHidePassword] = useState(false);
    const usersList = useQuery(["get-users-list"], async () => request.get(`/admin/users`));
    const deleteUser = useMutation(({id}: { id: string }) => request.delete(`/admin/delete-user/${id}`))


    const handleDeleteUser = (record: any) => {
        if (record?.isAdmin) {
            return message.info("У вас нет прав на удаление администратора")
        }
        const deleteAsync = deleteUser.mutateAsync({id: record?._id})
        deleteAsync.then(() => {
            message.success("Пользователь успешно удален");
            usersList.refetch();
        }).catch(() => {
            usersList.refetch();
            message.error("Ошибка удаления пользователя");
        })
    }

    const columns = [
        {
            key: "index",
            title: "№",
            dataIndex: "index",
            render: (text: string, record: any, index: number) => index + 1
        },
        {
            title: 'Имя',
            dataIndex: 'firstName',
            key: 'firstName',
        },
        {
            title: 'Фамилия',
            dataIndex: 'lastName',
            key: 'lastName',
        },
        {
            title: 'Телефонный номер',
            dataIndex: 'msisdn',
            key: 'msisdn',
        },
        {
            title: 'Эл. адрес',
            dataIndex: 'email',
            key: 'email',
        },
        // {
        //     title: 'Пароль',
        //     dataIndex: 'password',
        //     key: 'password',
        //     render: (text: string, record: any, index: number) => <div
        //         style={{display: "flex", justifyContent: "space-between"}}>
        //         <div>
        //             <p>{hidePassword ? record.password : "********"}</p>
        //         </div>
        //         <Button onClick={() => setHidePassword(!hidePassword)}>
        //             {hidePassword ? <EyeInvisibleOutlined/> : <EyeOutlined/>}
        //         </Button>
        //     </div>,
        //     width: 300
        // },
        {
            title: 'Действия',
            dataIndex: 'actions',
            key: 'actions',
            render: (text: string, record: any, index: number) => <div>
                {/*<Button className="rounded-2xl" type="text"> <EditOutlined/> </Button>*/}
                {!record?.isAdmin &&
                    <Button
                        onClick={() => handleDeleteUser(record)}
                        className="rounded-2xl"
                        type="text"
                    >
                        {deleteUser?.isLoading ? <Spin/> : <DeleteOutlined/>}
                    </Button>}
            </div>,
            width: 100
        },
    ];

    return (
        <div>
            <h1 className="text-2xl font-bold mb-4">Пользователи</h1>
            <Table loading={usersList?.isLoading} dataSource={usersList?.data?.data} columns={columns}/>
        </div>
    )
}
export default Users;
