import React, {useEffect, useState} from "react";
import {Button, Col, Form, Input, message, Select, Table} from "antd";
import {DeleteOutlined, MinusCircleOutlined, PlusOutlined} from '@ant-design/icons';
import {useMutation, useQuery} from "react-query";
import {request} from "../../services/Api";


const AddCategory = () => {
    const [form] = Form.useForm();
    const [name, setCategoryValue] = useState("");
    const addCategoryMutate = useMutation((data: any) => request.post(`/admin/add-category`, data), {
        onSuccess: () => {
            form.resetFields()
        }
    })
    const addSubCategoryMutate = useMutation((data: any) => request.post(`/admin/sub-category`, data), {
        onSuccess: () => {
            form.resetFields()
        }
    })
    const deleteCategory = useMutation(({_id}: { _id: string }) => request.delete(`/admin/delete-category/${_id}`))
    const deleteSubCategory = useMutation(({_id}: { _id: string }) => request.delete(`/admin/delete-subcategory/${_id}`))
    const getCategoryList = useQuery(["get-category-list"], async () => request.get(`/admin/categories`))
    const [categoryId, setCategoryId] = useState("");
    const getSubcategoryList = useQuery(["get-subcategory-list", categoryId], async () => request.get(`/admin/sub-categories/${categoryId}`), {enabled: categoryId !== ""})

    const options = getCategoryList?.data?.data?.map((data: any, index: number) => {
        return {
            label: data?.name,
            value: data?._id
        }
    });

    const handleDeleteCategory = (id: string) => {
        const deleteAsync = deleteCategory.mutateAsync({_id: id})
        deleteAsync.then(() => {
            message.success("Пользователь успешно удален");
            getCategoryList.refetch();
        }).catch(() => {
            getCategoryList.refetch();
            message.error("Ошибка удаления пользователя");
        })
    }
    const handleDeleteSubcategory = (id: string) => {
        const deleteAsync = deleteSubCategory.mutateAsync({_id: id})
        deleteAsync.then(() => {
            message.success("Пользователь успешно удален");
            getSubcategoryList.refetch();
        }).catch(() => {
            getSubcategoryList.refetch();
            message.error("Ошибка удаления пользователя");
        })
    }

    const onFinish = (values: any) => {
        const addSubCategory = addSubCategoryMutate.mutateAsync(values?.categories);
        addSubCategory.then((res) => {
            message.success("Категория успешно создана")
        }).catch((err) => {
            message.error("Ошибка при создании категории")
        })
    };

    const handleAddCategory = () => {
        const categoryMutateAsync = addCategoryMutate.mutateAsync({name})
        categoryMutateAsync.then((res) => {
            getCategoryList.refetch();
            message.success("Категория успешно создана")
            setCategoryValue("")
        }).catch((err) => {
            message.error("Ошибка при создании категории")
        })
    }
    const categoryColumn = [
        {
            title: 'Категории',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Действия',
            dataIndex: 'actions',
            key: 'actions',
            render: (text: string, record: any, index: number) => <div>
                {/*<Button className="rounded-2xl" type="text"> <EditOutlined/> </Button>*/}
                <Button style={{color: "red"}} onClick={() => handleDeleteCategory(record?._id)} className="rounded-2xl"
                        type="text"><DeleteOutlined/></Button>
            </div>
        },
    ];
    const subcategoryColumn = [
        {
            title: 'Категории',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Действия',
            dataIndex: 'actions',
            key: 'actions',
            render: (text: string, record: any, index: number) => <div>
                {/*<Button className="rounded-2xl" type="text"> <EditOutlined/> </Button>*/}
                <Button style={{color: "red"}} onClick={() => handleDeleteSubcategory(record?._id)}
                        className="rounded-2xl"
                        type="text"><DeleteOutlined/></Button>
            </div>
        },
    ];


    return (
        <div>
            <div style={{borderBottom: "1px solid rgba(0,21,41, 0.5)", paddingBottom: 20}}>
                <Input.Group compact style={{display: "flex"}}>
                    <Input onChange={(e: any) => setCategoryValue(e?.target?.value)} placeholder="Добавить категорию"
                           name="name" value={name}/>
                    <Button onClick={handleAddCategory} type="primary">Добавить</Button>
                </Input.Group>
            </div>
            <div style={{paddingTop: 20}}>
                <Form form={form} name="category-adder" onFinish={onFinish} autoComplete="off">
                    <Form.List name="categories">
                        {(fields, {add, remove}) => (
                            <>
                                {fields.map(({key, name, ...restField}) => (
                                    <div key={key} style={{display: "flex", justifyContent: "space-between"}}>
                                        <Col span={22} style={{display: "flex", justifyContent: "space-between"}}>
                                            <Form.Item
                                                {...restField}
                                                name={[name, "categoryId"]}
                                                style={{width: "100%", marginRight: 10}}
                                            >
                                                <Select
                                                    placeholder="Список категорий"
                                                    options={options}
                                                    // filterOption={(input, option) =>
                                                    //     option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                                    // }
                                                    // filterSort={(optionA, optionB) =>
                                                    //     optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
                                                    // }
                                                />
                                            </Form.Item>
                                            <Form.Item
                                                {...restField}
                                                name={[name, "name"]}
                                                style={{width: "100%", marginLeft: 10}}
                                            >
                                                <Input placeholder="Название подкатегории"/>
                                            </Form.Item>
                                        </Col>
                                        <Col span={1}>
                                            <MinusCircleOutlined onClick={() => remove(name)}/>
                                        </Col>
                                    </div>
                                ))}
                                <Form.Item>
                                    <Button  type="dashed" onClick={() => add()} block icon={<PlusOutlined/>}>
                                        Добавить подкатегорию
                                    </Button>
                                </Form.Item>
                            </>
                        )}
                    </Form.List>
                    <Form.Item>
                        <Button loading={addSubCategoryMutate?.isLoading} style={{float: "right"}} type="primary" htmlType="submit">
                            Добавлять
                        </Button>
                    </Form.Item>
                </Form>
            </div>
            <h1 className="text-2xl text-center">Список категорий</h1>
            <Table
                style={{marginBottom: 30}}
                dataSource={getCategoryList?.data?.data}
                loading={getCategoryList?.isLoading}
                columns={categoryColumn}
                className={"mb-60"}
                size={"middle"}
                pagination={false}
            />
            <div>
                <h1 className="text-2xl text-center">Список подкатегорий</h1>
                <div style={{display: "flex", flexDirection: "column"}}>
                    <label style={{marginBottom: 5}} htmlFor="select">Пожалуйста, выберите категории</label>
                    <Select onChange={(val: string) => setCategoryId(val)} style={{width: 300, marginBottom: 20}}
                            placeholder={"Пожалуйста, выберите категории"} defaultValue={options?.[0]?.value}
                            value={categoryId} id="select" options={options}/>
                </div>
                <Table
                    dataSource={getSubcategoryList?.data?.data}
                    columns={subcategoryColumn}
                    className={"mb-60"}
                    loading={getSubcategoryList?.isLoading}
                    size={"middle"}
                    pagination={false}
                />
            </div>
        </div>
    )
}
export default AddCategory;
