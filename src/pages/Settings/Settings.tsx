import React from "react";
import {Button, Col, Form, Input, Row} from "antd";

const Settings = () => {

    const onFinish = (values: any) => {
        console.log('Success:', values);
    };

    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <div>
            <Form
                name="basic"
                labelCol={{span: 20}}
                wrapperCol={{span: 23}}
                initialValues={{remember: true}}
                layout="vertical"
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
            >
                <Row justify={"end"}>
                    <Col md={8}>
                        <Form.Item
                            label="Текущий пароль"
                            name="currentPassword"
                        >
                            <Input.Password/>
                        </Form.Item>
                    </Col>
                    <Col md={8}>
                        <Form.Item
                            label="Новый пароль"
                            name="newPassword"
                        >
                            <Input.Password/>
                        </Form.Item>
                    </Col>
                    <Col md={8}>
                        <Form.Item label="Подтвердите новый пароль" name="confirmNewPassword">
                            <Input.Password/>
                        </Form.Item>
                    </Col>
                    <Col md={5}>
                        <Form.Item wrapperCol={{offset: 8, span: 16}}>
                            <Button type="primary" htmlType="submit">
                                Обновить пароль
                            </Button>
                        </Form.Item>
                    </Col>
                </Row>

            </Form>
        </div>
    )
}
export default Settings;
