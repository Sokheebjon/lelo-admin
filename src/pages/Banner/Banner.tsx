import React, {useState} from "react"
import {Table, Upload, message, Button, Radio, Select} from "antd";
import {useQuery} from "react-query";
import {request} from "../../services/Api";
import {UploadOutlined} from '@ant-design/icons';


const Banner = () => {
    // const [gender, setGender] = useState("boys")
    const [categoryId, setCategoryId] = useState<any>("");
    const [subCatId, setSubCatId] = useState("");
    const [file, setFile] = useState()
    const [isLoading, setLoading] = useState(false)
    const [imgUrl, setImgUrl] = useState({
        imgIndex: -1,
        imgUrl: ""
    })
    const formData: any = new FormData();
    const props = {
        beforeUpload: (file: any) => {
            const isPNG = file.type === 'image/png' || file.type === 'image/jpg' || file.type === 'image/jpeg' || file.type === 'image/webp';
            if (!isPNG) {
                message.error(`Извините, вы можете сохранить только изображение`);
            }
            return isPNG || Upload.LIST_IGNORE;
        },
        maxCount: 1
    };

    function getBase64(img: any, callback: any) {
        const reader = new FileReader();
        reader.addEventListener('load', () => callback(reader.result));
        reader.readAsDataURL(img);
    }

    // const handleGenderChange = ()=> {
    //
    // }
    const handleUploadImage = (e: any, index: number) => {
        setFile(e?.file?.originFileObj)
        // Get this url from response in real world.
        getBase64(e.file.originFileObj, (imageUrl: any) =>
            setImgUrl({imgUrl: imageUrl, imgIndex: index})
        );
    }

    const handleUpdateBanner = (id: string) => {
        if (categoryId === "") {
            return message.error("Название категории отсутствует")
        }
        if (subCatId === "") {
            return message.error("Название подкатегории отсутствует")
        }
        if (!file) {
            return message.error("Изображение отсутствует")
        }
        file && formData?.append(`photo`, file);
        subCatId && formData?.append(`tag`, subCatId);
        categoryId && formData?.append(`categoryId`, categoryId);
        setLoading(true);
        request.put(`/admin/update/banner/${id}`, formData, {
            headers: {
                "Content-Type": "multipart/form-data; boundary=---WebKitFormBoundary7MA4YWxkTrZu0gW"
            }
        }).then(data => {
            setLoading(false);
            message.success("Баннер успешно изменен");
        }).catch((error) => {
            message.error("Ошибка изменения баннера")
            setLoading(false);
        });
    }

    const getBanners = useQuery(["get-banner-images"], async () => request.get(`/admin/list/banners`))
    const getCategoryList = useQuery(["get-category-list"], async () => request.get(`/admin/categories`))
    const getSubCategoryList = useQuery(["get-subcategory-list", categoryId], async () => request.get(`/admin/sub-categories/${categoryId}`), {enabled: categoryId !== ""})
    const options = getCategoryList?.data?.data?.map((data: any, index: number) => {
        return {
            label: data?.name,
            value: data?._id
        }
    });

    const subOptions = getSubCategoryList?.data?.data?.map((data: any, index: number) => {
        return {
            label: data?.name,
            value: data?._id
        }
    });
    const handleSelectCategory = (e: any) => {
        setCategoryId(e);
    }
    const handleSelectSubcategory = (e: any) => {
        setSubCatId(e);
    }

    const columns = [
        {
            key: "count",
            title: "№",
            dataIndex: "count",
            render: (text: string, record: any, index: number) => index + 1
        },
        {
            key: "photo",
            title: "Изображение",
            dataIndex: "photo",
            render: (text: string, record: any, index: number) => <div
                className="flex justify-evenly items-center">
                {imgUrl?.imgIndex === index ?
                    <img src={imgUrl?.imgUrl} width={"300px"} alt={record?.tag}/> :
                    <img src={record?.photo} width={"300px"} alt={record?.tag}/>
                }

                <Upload accept={"image/png, image/webp, image/jpg, image/jpeg"} {...props}
                        onChange={(e) => handleUploadImage(e, index)}>
                    <Button icon={<UploadOutlined/>}>Измени фотографию</Button>
                </Upload>
            </div>,
        },
        // {
        //     key: "gender",
        //     title: "Мальчики/девочки",
        //     dataIndex: "gender",
        //     render: (text: string, record: any, index: number) => <div>
        //         <Radio.Group onChange={handleGenderChange} value={gender}>
        //             <Radio value={1}>A</Radio>
        //             <Radio value={2}>B</Radio>
        //         </Radio.Group>
        //     </div>
        // },
        {
            key: "tag",
            title: "Название категории",
            dataIndex: "tag",
            render: (text: string, record: any, index: number) => (
                <div>
                    <Select onSelect={handleSelectCategory} style={{width: 150}}
                            options={options}/>
                </div>
            )
        },
        {
            key: "tag",
            title: "Название подкатегории",
            dataIndex: "tag",
            render: (text: string, record: any, index: number) => (
                <div>
                    <Select disabled={categoryId === ""} loading={getCategoryList?.isLoading}
                            style={{width: 150}} onSelect={handleSelectSubcategory} options={subOptions}/>
                </div>
            )
        },
        {
            key: "action",
            title: "Действие",
            dataIndex: "action",
            render: (text: string, record: any, index: number) => (
                <div>
                    <Button loading={isLoading} onClick={() => handleUpdateBanner(record?._id)}
                            type="primary">Обновить</Button>
                </div>
            )
        }

    ]

    return (
        <div>
            <h1 className="text-2xl font-bold mb-4">Баннер</h1>
            <Table loading={getBanners?.isLoading} columns={columns} dataSource={getBanners?.data?.data}/>
        </div>
    );
}
export default Banner;