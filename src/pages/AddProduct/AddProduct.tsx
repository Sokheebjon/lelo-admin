import React, {useState} from "react";
import {Button, Col, Form, Input, message, Row, Select, Upload} from "antd";
import {InboxOutlined} from '@ant-design/icons';
import {useMutation, useQuery} from "react-query";
import {request} from "../../services/Api";
import {BlockPicker} from 'react-color';


interface FormTypes {
    categoryId?: string;
    subcategoryId?: string;
    title?: string;
    price?: number;
    salePrice?: number;
    description?: string;
    photos?: any;
    color?: Array<any>;
    ages?: Array<any>;
    eachPack?: string | number;
}

const AddProduct = () => {
    // const addProductMutation = useMutation((data) => request.post(`/admin/add-product`, data))
    const [form] = Form.useForm();
    const [categoryId, setCategoryId] = useState<any>();
    const [loading, setLoading] = useState(false);
    const formData: any = new FormData();
    const [enabled, setEnabled] = useState(false);
    const [photos, setPhotos] = useState<Array<any>>([]);
    const getCategoryList = useQuery(["get-category-list"], async () => request.get(`/admin/categories`))
    const getSubCategoryList = useQuery(["get-subcategory-list", categoryId], async () => request.get(`/admin/sub-categories/${categoryId}`), {enabled})
    const onFinish = async ({
                                categoryId,
                                subcategoryId,
                                title,
                                price,
                                // salePrice,
                                description,
                                color = [],
                                ages = [],
                                eachPack
                            }: FormTypes) => {
        formData.append(`categoryId`, categoryId)
        formData.append(`subcategoryId`, subcategoryId)
        formData.append(`title`, title)
        formData.append(`price`, price)
        // formData.append(`salePrice`, salePrice)
        formData.append(`description`, description)
        formData.append(`eachPack`, eachPack)
        for (let i = 0; i < photos.length; i++) {
            await formData?.append(`photos`, photos[i]?.originFileObj);
        }
        color.forEach(col => formData.append('colors[]', col))
        ages.forEach(age => formData.append('ages[]', age))
        setLoading(true)

        request.post("/admin/add-product ", formData, {
            headers: {
                "Content-Type": "multipart/form-data; boundary=---WebKitFormBoundary7MA4YWxkTrZu0gW"
            }
        }).then(data => {
            setLoading(false);
            form.resetFields();
            message.success("товар успешно создан");
        }).catch((error) => {
            message.error("Ошибка добавления товара")
            setLoading(false);
        });
    };
    const handlePhotos = async (info: any) => {
        setPhotos(info?.fileList);
    };

    const options = getCategoryList?.data?.data?.map((data: any, index: number) => {
        return {
            label: data?.name,
            value: data?._id
        }
    });
    const colorOptions = [
        {
            label: "Красный",
            value: "Красный&#FF0000"
        },
        {
            label: "Синий",
            value: "Синий&#0000FF"
        },
        {
            label: "Розовый",
            value: "Розовый&#FFC0CB"
        },
        {
            label: "Желтый",
            value: "Желтый&#FFFF00"
        },
        {
            label: "Фиолетовый",
            value: "Фиолетовый&#8F00FF"
        },
        {
            label: "Апельсин",
            value: "Апельсин&#FFA500"
        },
        {
            label: "Коричневый",
            value: "Коричневый&#A52A2A"
        },
        {
            label: "Черный",
            value: "Черный&#000000"
        },
    ];
    const ageOptions = [
        {
            label: "4 лет",
            value: 4
        },
        {
            label: "5 лет",
            value: 5
        },
        {
            label: "6 лет",
            value: 6
        },
        {
            label: "7 лет",
            value: 7
        },
        {
            label: "8 лет",
            value: 8
        },
        {
            label: "9 лет",
            value: 9
        }
    ]

    const subcategoryOptions = getSubCategoryList?.data?.data?.map((data: any, index: number) => {
        return {
            label: data?.name,
            value: data?._id
        }
    });
    const handleChangeValues = (changedValues: FormTypes, allValues: FormTypes) => {
        setEnabled(true);
        setCategoryId(allValues?.categoryId);
    }
    const {TextArea} = Input;
    const {Dragger} = Upload;

    return (
        <div>
            <Form
                form={form}
                name="basic"
                labelCol={{span: 24}}
                wrapperCol={{span: 24}}
                initialValues={{remember: true}}
                onFinish={onFinish}
                onValuesChange={handleChangeValues}
                layout="vertical"
            >
                <Row gutter={[20, 20]}>
                    <Col md={8}>
                        <Form.Item
                            name={"categoryId"}
                            style={{width: "100%", marginRight: 10}}
                            label="Какая категория"
                            rules={[{required: true, message: 'Пожалуйста, выберите категорию!'}]}
                        >
                            <Select
                                options={options}
                                // filterOption={(input, option) =>
                                //     option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                // }
                                // filterSort={(optionA, optionB) =>
                                //     optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
                                // }
                            />
                        </Form.Item>
                    </Col>
                    <Col md={8}>
                        <Form.Item
                            name={"subcategoryId"}
                            style={{width: "100%", marginRight: 10}}
                            label="Какая подкатегория"
                            rules={[{required: true, message: 'Пожалуйста, выберите подкатегория!'}]}
                        >
                            <Select
                                options={subcategoryOptions}
                                disabled={!getCategoryList?.isSuccess}
                                // filterOption={(input, option) =>
                                //     option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                // }
                                // filterSort={(optionA, optionB) =>
                                //     optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
                                // }
                            />
                        </Form.Item>
                    </Col>
                    <Col md={8}>
                        <Form.Item
                            label="Наименование товара"
                            name="title"
                            rules={[{required: true, message: 'Пожалуйста, введите название товара!'}]}
                        >
                            <Input/>
                        </Form.Item>
                    </Col>
                    <Col md={8}>
                        <Form.Item
                            label="Цена товара"
                            name="price"
                            rules={[{required: true, message: 'Пожалуйста, введите цена товара!'}]}
                        >
                            <Input/>
                        </Form.Item>
                    </Col>
                    {/*<Col md={8}>*/}
                    {/*    <Form.Item*/}
                    {/*        label="Цена в продаже"*/}
                    {/*        name="salePrice"*/}
                    {/*    >*/}
                    {/*        <Input/>*/}
                    {/*    </Form.Item>*/}
                    {/*</Col>*/}
                    <Col md={8}>
                        <Form.Item
                            label="Цвета товара"
                            name="color"
                        >
                            <Select
                                options={colorOptions}
                                mode="multiple"
                            />
                        </Form.Item>
                    </Col>
                    <Col md={8}>
                        <Form.Item
                            label="Возрастные товары"
                            name="ages"
                        >
                            <Select
                                options={ageOptions}
                                mode="multiple"
                            />
                        </Form.Item>
                    </Col>
                    <Col md={8}>
                        <Form.Item
                            label="В упак."
                            name="eachPack"
                            // rules={[{required: true, message: 'Пожалуйста, введите каждая упаковка!'}]}
                        >
                            <Input type="number"/>
                        </Form.Item>
                    </Col>
                    <Col md={8}>
                        <Form.Item
                            label="Описание товара"
                            name="description"
                        >
                            <TextArea rows={2}/>
                        </Form.Item>
                    </Col>
                    <Col md={24}>
                        <Form.Item
                            label="Изображения товара"
                            name="photos"
                        >
                            <Dragger
                                multiple={true}
                                onChange={handlePhotos}
                                accept="image/png, image/gif, image/jpeg"
                            >
                                <p className="ant-upload-drag-icon">
                                    <InboxOutlined/>
                                </p>
                                <p className="ant-upload-text">Нажмите или перетащите файл в эту область, чтобы
                                    загрузить</p>
                            </Dragger>
                        </Form.Item>
                    </Col>
                </Row>
                <Form.Item style={{float: 'right'}}>
                    <Button loading={loading} type="primary" htmlType="submit">
                        Добавить товар
                    </Button>
                </Form.Item>
            </Form>
        </div>
    )
}

export default AddProduct;
