import React from "react";
import {Button, Row, Col, Form, Input, message} from "antd";
import {UserOutlined, LockOutlined} from '@ant-design/icons';
import {useMutation} from "react-query";
import {request} from "../../services/Api";
import {setCookie} from "../../utils/cookie";

const Login = () => {
    const setAdmin = useMutation((data: any) => request.post(`/user/login`, data))

    const onFinish = (values: any) => {
        const setAdminAsync = setAdmin.mutateAsync({...values})
        setAdminAsync.then((res) => {
            if (res?.status === 200 || res?.status === 201) {
                setCookie("token", res?.data?.token)
                setCookie("user", JSON.stringify(res?.data?.user))
                window.location.href = "/";
                window.location.reload();
            } else {
                message.error("Электронная почта или пароль неверны")
            }
        }).catch((err) => {
            message.error("Электронная почта или пароль неверны")
        })
    };

    return (
        <div className="drop-shadow-lg">
            <Row justify="space-around">
                <Col span={7} style={{margin: "auto"}}>
                    <p className="text-3xl text-center font-bold">Авторизоваться</p>
                    <Form
                        name="normal_login"
                        className="login-form"
                        initialValues={{remember: true}}
                        onFinish={onFinish}
                    >
                        <Form.Item
                            name="email"
                            rules={[{required: true, message: 'Пожалуйста, введите ваше Эл. адрес!'}]}
                        >
                            <Input type="email" prefix={<UserOutlined className="site-form-item-icon"/>}
                                   placeholder="Email"/>
                        </Form.Item>
                        <Form.Item
                            name="password"
                            rules={[{required: true, message: 'Пожалуйста, введите Ваш пароль!'}]}
                        >
                            <Input
                                prefix={<LockOutlined className="site-form-item-icon"/>}
                                type="password"
                                placeholder="Password"
                            />
                        </Form.Item>
                        <Form.Item>
                            <Button loading={setAdmin?.isLoading} type="primary" style={{width: "100%"}}
                                    htmlType="submit" className="login-form-button block">
                                Логин
                            </Button>
                        </Form.Item>
                    </Form>
                </Col>
            </Row>
        </div>
    )
}

export default Login;
