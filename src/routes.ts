import {lazy} from "react";

const AddCategory = lazy(() => import("./pages/AddCategory"));
const AddProduct = lazy(() => import("./pages/AddProduct"));
const Dashboard = lazy(() => import("./pages/Dashboard"));
const Settings = lazy(() => import("./pages/Settings"));
const Users = lazy(() => import("./pages/Users"));

export const routes = [
    {
        index: true,
        element: Dashboard
    },
    {
        path: "add-category",
        element: AddCategory,
    },
    {
        path: "add-product",
        element: AddProduct
    },
    {
        path: "settings",
        element: Settings
    },
    {
        path: "users",
        element: Users
    },

]
