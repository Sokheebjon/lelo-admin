import axios from "axios";

import {QueryClient} from "react-query";

import {getCookie, removeCookie} from "../utils/cookie";

export const request = axios.create({
    baseURL: "https://lelokid.herokuapp.com/api",
    headers: {
        "auth-user": getCookie("token")
        // // common: {
        // Authorization: "auth-user" + getCookie("token")
        // // }
    }
});
request.interceptors.response.use(
    (response) => {
        return response;
    },
    (error) => {
        if (error.response.status === 401) {
            removeCookie("token");
            window.location.reload();
            window.location.pathname = "/";
        }
        return Promise.reject(error);
    }
);
request.interceptors.request.use(config => {
    config.headers = {
        ...config.headers,
        // common: {
        "auth-user": getCookie("token")
        // }
    }
    return config;
}, error => Promise.reject(error));


export const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            refetchOnWindowFocus: false
        }
    }
});
